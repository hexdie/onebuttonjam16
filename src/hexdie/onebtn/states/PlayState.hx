package hexdie.onebtn.states;

import hexdie.onebtn.managers.CollisionManager;
import hexdie.onebtn.components.PlayerControls;
import hexdie.onebtn.entity.ProjectileLauncher;
import hexdie.onebtn.managers.BackgroundManager;
import hexdie.onebtn.entity.Circler;
import hexdie.onebtn.entity.EntityBase;
import hexdie.onebtn.managers.EntityManager;
import hexdie.onebtn.entity.Platform;
import hexdie.onebtn.managers.ObstacleManager;
import hexdie.onebtn.managers.PlatformManager;
import hexdie.onebtn.entity.Player;
import hexdie.onebtn.event.GameEvents;
import hexdie.onebtn.event.LandingEvent;
import hexdie.onebtn.types.GameplayProperties;
import luxe.Color;
import luxe.Entity;
import luxe.Quaternion;
import luxe.Rectangle;
import luxe.Scene;
import luxe.Sprite;
import luxe.Text;
import luxe.Vector;
import luxe.collision.Collision;
import luxe.collision.shapes.Polygon;
import luxe.collision.shapes.Shape;
import luxe.components.sprite.SpriteAnimation;
import luxe.options.StateOptions;
import luxe.States.State;
import luxe.tween.Actuate;
import phoenix.Texture.FilterType;

/**
 * ...
 * @author 
 */
class PlayState extends State
{
	var player:EntityBase;
	var hitboxColor:Color;
	var playerControls:PlayerControls;
	var platformManager:PlatformManager;
	var projectileLauncher:ProjectileLauncher;
	var collisionManager:CollisionManager;
	var backgroundManager:BackgroundManager;
	var obstacleManager:ObstacleManager;
	var defaultCameraSpeed:Float = 3;
	var scoreText:Text;
	var deathText:Text;
	var deathScoreText:Text;
	var hearts:Array<Sprite>;
	var heartsBaseScreenPosition:Vector;
	var playerDeathConfigured:Bool;
	
	public static var props(default, null):GameplayProperties = {
		cameraSpeed: 0,
		projectileDistance: 300,
		projectileSpeed: 6,
		launcherFrequency: 0.375,
		circlerAngle: 1,
		circlerFrequency: 0.5,
		score: 0,
		playerHealth: 3
	};

	public function new(_options:StateOptions) 
	{
		super(_options);
	}
	
	override public function init() 
	{
		super.init();
		
		//Luxe.showConsole(true);
		
		player = new Player();
		playerControls = player.get("controls");
		platformManager = new PlatformManager(player.pos.x);
		hitboxColor = new Color().rgb(0xffffff);
		
		Luxe.camera.pos.copy_from(player.pos);
		Luxe.camera.pos.x -= 210;
		Luxe.camera.pos.y -= 100;
		
		collisionManager = new CollisionManager();
		backgroundManager = new BackgroundManager();
		obstacleManager = new ObstacleManager(platformManager);
		
		scoreText = new Text({
			text: Std.string(PlayState.props.score),
			color: new Color(),
			point_size: 20
		});
		
		deathText = new Text({
			text: "YOU CROAKED!",
			color: new Color(),
			point_size: 60,
			visible: false
		});
		
		deathScoreText = new Text({
			color: new Color(),
			point_size: 45,
			visible: false
		});
		
		hearts = new Array<Sprite>();
		heartsBaseScreenPosition = new Vector(12, 20);
		
		var heartTexture = Luxe.resources.texture("assets/images/heart.png");
		heartTexture.filter_mag = heartTexture.filter_min = FilterType.nearest;
		
		for (i in 0...props.playerHealth)
		{
			hearts.push(new Sprite({
				name: 'heart-$i',
				texture: heartTexture,
				size: new Vector(20, 20)
			}));
		}
		
		var startedCamera = false;
		
		Luxe.events.listen(GameEvents.PLAYER_JUMPED, function(_){
			if (!startedCamera)
			{
				startedCamera = true;
				Actuate.tween(props, 2.5, {cameraSpeed: defaultCameraSpeed});
				Luxe.audio.loop(Luxe.resources.audio("assets/audio/apple_pie.mp3").source, 0.7);
			}
		});
		
		playerDeathConfigured = false;
		
		Luxe.events.listen(GameEvents.PLAYER_DEATH, function(_){
			
			if (!playerDeathConfigured)
			{
				playerDeathConfigured = true;
				props.cameraSpeed = 0;
				player.velocity.set_xy( -5, -15);
				
				var anim:SpriteAnimation = player.get("animation");
				anim.animation = "death";
				anim.restart();
				
				scoreText.visible = false;
				deathText.visible = true;
				deathScoreText.text = "Your score: " + PlayState.props.score;
				deathScoreText.visible = true;
			}
		});
	}
	
	override public function update(dt:Float) 
	{
		super.update(dt);
		
		Luxe.camera.pos.x += props.cameraSpeed;
		
		collisionManager.update();
		backgroundManager.update();
		obstacleManager.update();
		updateHearts();
		
		scoreText.text = Std.string(PlayState.props.score);
		scoreText.pos.copy_from(Luxe.camera.screen_point_to_world(new Vector(12, 35)));
		
		var deathTextX = (Luxe.screen.width / 2) - 200;
		deathText.pos.copy_from(Luxe.camera.screen_point_to_world(new Vector(deathTextX, 20)));
		deathScoreText.pos.copy_from(Luxe.camera.screen_point_to_world(new Vector(deathTextX + 65, Luxe.screen.height - 100)));
		
		#if debug
		drawHitbox(cast player.hitbox, hitboxColor);
		#end
		
		var playerScreenPos = Luxe.camera.world_point_to_screen(player.hitbox.position);
		
		if (playerScreenPos.x < -30 || playerScreenPos.y > 700)
		{
			props.playerHealth = 0;
			Luxe.events.fire(GameEvents.PLAYER_DEATH);
		}
	}
	
	private function updateHearts()
	{
		var worldPos = Luxe.camera.screen_point_to_world(heartsBaseScreenPosition);
		var index = 0;
		
		for (h in hearts)
		{
			var heart:Sprite = h;
			heart.pos.copy_from(worldPos);
			heart.pos.x += ((heart.size.x + 2) * index) + 10;
			heart.visible = index < props.playerHealth;
			index += 1;
		}
	}
	
	public static function drawHitbox(poly:Polygon, color:Color)
	{
		var quaternion = new Quaternion();
		quaternion.setFromAxisAngle(new Vector(0, 0, 1), poly.rotation);
		
		var geom = Luxe.draw.poly({
			solid: false,
			close: true,
			depth: 100,
			points: poly.vertices,
			immediate: true,
			color: color,
			rotation: quaternion
		});
		
		geom.transform.pos.copy_from(poly.position);
	}
	
}