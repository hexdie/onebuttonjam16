package hexdie.onebtn.components;

import hexdie.onebtn.entity.Player;
import hexdie.onebtn.event.GameEvents;
import luxe.Component;
import luxe.components.sprite.SpriteAnimation;
import luxe.options.ComponentOptions;

/**
 * ...
 * @author 
 */
class PlayerAnimationController extends Component
{
	var player:Player;
	var anim:SpriteAnimation;
	
	public function new(?_options:ComponentOptions) 
	{
		super({name: "animation-controller"});
	}
	
	override public function init() 
	{
		super.init();
		
		player = cast entity;
		anim = player.get("animation");
		
		Luxe.events.listen(GameEvents.PLAYER_JUMPED, function(_){
			anim.animation = "jump";
			anim.play();
			
			Luxe.audio.play(Luxe.resources.audio("assets/audio/jump.mp3").source);
		});
		
		Luxe.events.listen(GameEvents.PLAYER_LANDED, function(_){
			anim.animation = "land";
			anim.play();
			
			//Luxe.audio.play(Luxe.resources.audio("assets/audio/land.mp3").source);
		});
		
		Luxe.events.listen(GameEvents.PLAYER_FLY_START, function(_){
			anim.animation = "fly";
			anim.play();
			
			Luxe.audio.play(Luxe.resources.audio("assets/audio/dash.mp3").source);
		});
		
		Luxe.events.listen(GameEvents.PLAYER_FLY_END, function(_){
			anim.animation = "fall";
			anim.play();
		});
	}
}