package hexdie.onebtn.components;

import hexdie.onebtn.entity.EntityBase;
import hexdie.onebtn.managers.EntityManager;
import hexdie.onebtn.event.GameEvents;
import luxe.Component;
import luxe.collision.Collision;
import luxe.options.ComponentOptions;

/**
 * ...
 * @author 
 */
class PlayerPhysics extends Component
{
	var player:EntityBase;
	var playerControls:PlayerControls;
	var gravity:Float;
	var gravityLimit:Float;

	public function new(?_options:ComponentOptions) 
	{
		super(_options);
		
		gravity = 0.75;
		gravityLimit = 20;
	}
	
	override public function init() 
	{
		super.init();
		
		player = cast entity;
		playerControls = player.get("controls");
	}
	
	override public function update(dt:Float) 
	{
		super.update(dt);
		
		if (!playerControls.dashing)
		{
			if (player.velocity.y < gravityLimit)
			{
				player.velocity.y += gravity;
			}
		}
	}
}