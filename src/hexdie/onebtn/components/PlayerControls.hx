package hexdie.onebtn.components;

import hexdie.onebtn.entity.EntityBase;
import hexdie.onebtn.event.GameEvents;
import hexdie.onebtn.states.PlayState;
import luxe.Component;
import luxe.Input.Key;
import luxe.Vector;
import luxe.options.ComponentOptions;

/**
 * ...
 * @author 
 */
class PlayerControls extends Component
{
	public var dashing(default, null):Bool;
	public var inAir(default, null):Bool;
	
	var owner:EntityBase;
	var jumpVelocity:Vector;
	var dashVelocity:Vector;
	var dashStartX:Float;
	
	public function new(?_options:ComponentOptions) 
	{
		super(_options);
	}
	
	override public function init() 
	{
		super.init();
		
		owner = cast entity;
		jumpVelocity = new Vector(0, -15);
		dashVelocity = new Vector(35, 0);
		inAir = false;
		dashing = false;
		
		Luxe.events.listen(GameEvents.PLAYER_LANDED, function(_) inAir = false);
	}
	
	override public function update(dt:Float) 
	{
		super.update(dt);
		
		if (PlayState.props.playerHealth > 0)
		{
			if (Luxe.input.keypressed(Key.space))
			{
				if (!inAir)
				{
					owner.velocity.add(jumpVelocity);
					inAir = true;
					
					Luxe.events.fire(GameEvents.PLAYER_JUMPED);
				}
				else 
				{
					dashStartX = owner.pos.x;
					owner.velocity.copy_from(dashVelocity);
					dashing = true;
					
					Luxe.events.fire(GameEvents.PLAYER_FLY_START);
				}
			}
			
			if (dashing)
			{
				var distanceTravelled = owner.pos.x - dashStartX;
				
				if (distanceTravelled >= 210)
				{
					dashing = false;
					owner.pos.x = dashStartX + 210;
					owner.velocity.x = 0;
					
					Luxe.events.fire(GameEvents.PLAYER_FLY_END);
				}
			}
		}
	}
	
}