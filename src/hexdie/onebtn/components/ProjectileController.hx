package hexdie.onebtn.components;

import hexdie.onebtn.entity.EntityBase;
import luxe.Component;
import luxe.options.ComponentOptions;

/**
 * ...
 * @author 
 */
class ProjectileController extends Component
{
	var owner:EntityBase;
	var speed:Float;
	
	public function new(speed:Float) 
	{
		super({name: "controller"});
		
		this.speed = speed;
	}
	
	override public function init() 
	{
		super.init();
		
		owner = cast entity;
		owner.velocity.y = Math.abs(speed) * -1;
	}
}