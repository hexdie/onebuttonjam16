package hexdie.onebtn.entity;
import hexdie.onebtn.managers.EntityManager;
import hexdie.onebtn.entity.Projectile;
import luxe.Entity;
import luxe.Vector;

/**
 * ...
 * @author 
 */
class ProjectileLauncher extends Entity
{
	private static var launcherNumber:Int = 0;
	public var projectiles(default, null):Array<Projectile>;
	var speed:Float;
	var projectileDistanceLimit:Float;
	var launcherActive:Bool;

	public function new(pos:Vector, speed:Float, distance:Float, delay:Float=0) 
	{
		launcherNumber += 1;
		
		super({
			pos: pos,
			name: 'projectile-launcher-$launcherNumber',
			scene: EntityManager.instance
		});
		
		this.projectiles = new Array<Projectile>();
		this.speed = speed;
		this.projectileDistanceLimit = distance;
		
		if (delay > 0)
		{
			launcherActive = false;
			Luxe.timer.schedule(delay, function() launcherActive = true);
		}
		else
		{
			launcherActive = true;
		}
	}
	
	override public function update(dt:Float) 
	{
		super.update(dt);
		
		if (launcherActive)
		{
			if (projectiles.length == 0)
			{
				projectiles.push(new Projectile(pos, speed));
			}
			
			if (projectiles[projectiles.length - 1].distanceTravelled > projectileDistanceLimit)
			{
				projectiles.push(new Projectile(pos, speed));
			}
		}
	}
}