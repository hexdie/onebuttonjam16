package hexdie.onebtn.entity;

import hexdie.onebtn.components.ProjectileController;
import hexdie.onebtn.managers.EntityManager;
import luxe.Vector;
import luxe.collision.shapes.Polygon;
import luxe.components.sprite.SpriteAnimation;
import luxe.options.SpriteOptions;
import phoenix.Texture.FilterType;

/**
 * ...
 * @author 
 */
class Projectile extends EntityBase
{
	private static var projectileNumber:Int = 0;
	private var initialY:Float;
	public var distanceTravelled(default, null):Float;

	public function new(pos:Vector, speed:Float, textureFile:String="assets/images/projectile-1.png") 
	{
		var texture = Luxe.resources.texture(textureFile);
		texture.filter_mag = texture.filter_min = FilterType.nearest;
		
		var spriteRotation = speed < 0 ? 90 : 270;
		
		projectileNumber += 1;
		
		super({
			name: 'projectile-$projectileNumber',
			texture: texture,
			pos: pos,
			size: new Vector(60, 60),
			scene: EntityManager.instance,
			rotation_z: spriteRotation
		});
		
		hitbox = Polygon.rectangle(pos.x, pos.y, 28, 38);
		
		add(new ProjectileController(speed));
		
		initialY = pos.y;
		tags.push("obstacle");
		
		var anim:SpriteAnimation = add(new SpriteAnimation({name: "animation"}));
		var animationData = Reflect.copy(Luxe.resources.json("assets/animations/fish-1.json").asset.json);
		anim.add_from_json_object(animationData);
		anim.animation = "default";
		anim.play();
	}
	
	override public function update(dt:Float) 
	{
		super.update(dt);
		
		distanceTravelled = Math.abs(hitbox.position.y - initialY);
	}
	
}