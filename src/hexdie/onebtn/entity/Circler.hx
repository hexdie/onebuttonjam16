package hexdie.onebtn.entity;

import hexdie.onebtn.managers.EntityManager;
import luxe.Vector;
import luxe.collision.shapes.Polygon;
import luxe.components.sprite.SpriteAnimation;
import luxe.options.SpriteOptions;
import phoenix.Texture.FilterType;

/**
 * ...
 * @author 
 */
class Circler extends EntityBase
{
	private static var circlerNumber:Int = 0;
	
	var rotationAngle:Float;
	var platform:Platform;

	public function new(platform:Platform, _rotationAngle:Float=1) 
	{
		var texture = Luxe.resources.texture("assets/images/bee.png");
		texture.filter_mag = texture.filter_min = FilterType.nearest;
		
		var pos:Vector = new Vector();
		pos.copy_from(platform.pos);
		
		circlerNumber += 1;
		
		super({
			name: 'circler-$circlerNumber',
			texture: texture,
			pos: new Vector(platform.pos.x, platform.pos.y),
			size: new Vector(32, 32),
			scene: EntityManager.instance
		});
		
		tags.push("obstacle");
		hitbox = Polygon.rectangle(pos.x, pos.y - 80, 20, 12);
		
		rotationAngle = _rotationAngle;
		this.platform = platform;
		
		var anim:SpriteAnimation = add(new SpriteAnimation({name: "animation"}));
		var animationData = Reflect.copy(Luxe.resources.json("assets/animations/bee.json").asset.json);
		anim.add_from_json_object(animationData);
		anim.animation = "default";
		anim.play();
	}
	
	override public function update(dt:Float) 
	{
		super.update(dt);
		
		if (platform != null && !platform.destroyed)
		{
			//http://www.gamefromscratch.com/post/2012/11/24/GameDev-math-recipes-Rotating-one-point-around-another-point.aspx
			var angle = (rotationAngle * dt) * (Math.PI);
			var rotatedX = Math.cos(angle) * (hitbox.position.x - platform.pos.x) - Math.sin(angle) * (hitbox.position.y - platform.pos.y) + platform.pos.x;
			var rotatedY = Math.sin(angle) * (hitbox.position.x - platform.pos.x) + Math.cos(angle) * (hitbox.position.y - platform.pos.y) + platform.pos.y;
			
			flipx = rotatedX < hitbox.position.x;
			
			hitbox.position.set_xy(rotatedX, rotatedY);
			pos.copy_from(hitbox.position);
		}
	}
	
}