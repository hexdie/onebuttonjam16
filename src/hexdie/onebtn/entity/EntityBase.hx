package hexdie.onebtn.entity;

import luxe.Vector;
import luxe.collision.shapes.Polygon;
import luxe.collision.shapes.Shape;
import luxe.options.SpriteOptions;
import luxe.Sprite;

/**
 * ...
 * @author 
 */
class EntityBase extends Sprite
{
	public var tags(default, null):Array<String>;
	public var hitbox(default, null):Shape;
	public var velocity(default, null):Vector;
	
	public function new(options:SpriteOptions) 
	{
		super(options);
		
		tags = new Array<String>();
		
		hitbox = Polygon.rectangle(pos.x, pos.y, size.x, size.y);
		hitbox.position.copy_from(pos);
		
		velocity = new Vector();
	}
	
	override public function update(dt:Float) 
	{
		super.update(dt);
		
		hitbox.position.add(velocity);
		pos.copy_from(hitbox.position);
	}
}