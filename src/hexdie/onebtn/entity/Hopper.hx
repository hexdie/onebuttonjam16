package hexdie.onebtn.entity;

import hexdie.onebtn.managers.EntityManager;
import luxe.Vector;
import luxe.options.SpriteOptions;

/**
 * ...
 * @author 
 */
class Hopper extends EntityBase
{
	public static var hopperNumber:Int;
	
	public function new(platform:Platform) 
	{
		super(options);
		
		hopperNumber += 1;
		
		var texture = Luxe.resources.texture(textureFile);
		texture.filter_mag = texture.filter_min = FilterType.nearest;
		
		var pos:Vector = platform.pos.clone();
		pos.y -= 80;
		
		super({
			name: 'hopper-$hopperNumber',
			texture: texture,
			pos: pos,
			size: new Vector(32, 32),
			scene: EntityManager.instance
		});
		
		hitbox = Polygon.rectangle(pos.x, pos.y, 28, 38);
	}
	
}