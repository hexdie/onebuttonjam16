package hexdie.onebtn.entity;

import hexdie.onebtn.components.PlayerAnimationController;
import hexdie.onebtn.components.PlayerControls;
import hexdie.onebtn.components.PlayerPhysics;
import hexdie.onebtn.managers.EntityManager;
import luxe.Vector;
import luxe.collision.shapes.Polygon;
import luxe.components.sprite.SpriteAnimation;
import luxe.options.SpriteOptions;
import phoenix.Texture.FilterType;

/**
 * ...
 * @author 
 */
class Player extends EntityBase
{

	public function new() 
	{
		var texture = Luxe.resources.texture("assets/images/player.png");
		texture.filter_mag = texture.filter_min = FilterType.nearest;
		
		super({
			name: "player",
			texture: texture,
			pos: new Vector((Luxe.camera.viewport.w * -0.5) + 16, 0),
			size: new Vector(48, 48),
			scene: EntityManager.instance
		});
		
		hitbox = Polygon.rectangle(pos.x, pos.y, 30, 30);
		
		var animationData = Reflect.copy(Luxe.resources.json("assets/animations/player.json").asset.json);
		var animationComponent:SpriteAnimation = add(new SpriteAnimation({name: "animation"}));
		animationComponent.add_from_json_object(animationData);
		
		animationComponent.animation = "default";
		animationComponent.play();
		
		add(new PlayerControls({name: "controls"}));
		add(new PlayerPhysics({name: "gravity"}));
		add(new PlayerAnimationController({name: "animation-controller"}));
	}
	
	override public function update(dt:Float) 
	{
		super.update(dt);
		
		pos.y -= 8;
	}
	
}