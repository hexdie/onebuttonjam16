package hexdie.onebtn.entity;

import hexdie.onebtn.managers.EntityManager;
import luxe.Vector;
import luxe.options.SpriteOptions;
import phoenix.Texture.FilterType;

/**
 * ...
 * @author 
 */
class Platform extends EntityBase
{
	private static var platformNumber:Int = 0;
	
	public function new(pos:Vector) 
	{
		platformNumber += 1;
		
		var texture = Luxe.resources.texture("assets/images/platform.png");
		texture.filter_mag = texture.filter_min = FilterType.nearest;
		
		super({
			name: 'platform-$platformNumber',
			texture: texture,
			pos: pos,
			size: new Vector(48, 24),
			scene: EntityManager.instance
		});
		
		tags.push("platform");
	}
}