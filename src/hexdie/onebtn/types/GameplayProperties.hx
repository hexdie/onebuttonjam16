package hexdie.onebtn.types;

/**
 * @author 
 */
typedef GameplayProperties =
{
	var cameraSpeed:Float;
	var projectileDistance:Float;
	var projectileSpeed:Float;
	var launcherFrequency:Float;
	var circlerAngle:Float;
	var circlerFrequency:Float;
	var score:Float;
	var playerHealth:Int;
}