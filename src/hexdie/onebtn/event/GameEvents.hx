package hexdie.onebtn.event;

/**
 * ...
 * @author 
 */
class GameEvents
{
	public static inline var PLAYER_LANDED:String = "PLAYER_LANDED";
	public static inline var PLAYER_JUMPED:String = "PLAYER_JUMPED";
	public static inline var PLAYER_FLY_START:String = "PLAYER_FLY_START";
	public static inline var PLAYER_FLY_END:String = "PLAYER_FLY_END";
	public static inline var PLAYER_DEATH:String = "PLAYER_DEATH";
	public static inline var RESTART_AVAILABLE:String = "RESTART_AVAILABLE";
}