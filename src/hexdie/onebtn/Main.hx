package hexdie.onebtn;

import hexdie.onebtn.event.GameEvents;
import hexdie.onebtn.managers.EntityManager;
import hexdie.onebtn.states.GameStates;
import hexdie.onebtn.states.PlayState;
import luxe.GameConfig;
import luxe.Input;
import luxe.Parcel;
import luxe.ParcelProgress;
import luxe.Screen.WindowEvent;
import luxe.States;

class Main extends luxe.Game 
{
	private var stateMachine:States;
	private var playState:State;
	
	public static var gameResolution = {
		x: 896,
		y: 504
	};
	
	override public function config(_config:GameConfig):GameConfig 
	{
		_config.window.width = gameResolution.x;
		_config.window.height = gameResolution.y;
		
		Luxe.snow.runtime.
		
		return super.config(_config);
	}
	
	override function ready() 
	{
		Luxe.camera.pos.add_xyz(gameResolution.x * -0.5, gameResolution.y * -0.5);
		
		var parcel = setupParcel();
		parcel.load();
	}
	
	private function setupParcel():Parcel
	{
		var data:Dynamic = {};
		
		data.textures = new Array<Dynamic>();
		data.jsons = new Array<Dynamic>();
		data.fonts = new Array<Dynamic>();
		data.sounds = new Array<Dynamic>();
		
		//textures
		data.textures.push({id: "assets/images/player.png"});
		data.textures.push({id: "assets/images/platform.png"});
		data.textures.push({id: "assets/images/projectile-1.png"});
		data.textures.push({id: "assets/images/background.png"});
		data.textures.push({id: "assets/images/bee.png"});
		data.textures.push({id: "assets/images/heart.png"});
		
		//json
		data.jsons.push({id: "assets/animations/player.json"});
		data.jsons.push({id: "assets/animations/fish-1.json"});
		data.jsons.push({id: "assets/animations/bee.json"});
		
		
		//audio
		data.sounds.push({id: "assets/audio/apple_pie.mp3"});
		data.sounds.push({id: "assets/audio/dash.mp3"});
		data.sounds.push({id: "assets/audio/jump.mp3"});
		data.sounds.push({id: "assets/audio/hit.mp3"});
		data.sounds.push({id: "assets/audio/land.mp3"});
		
		var parcel:Parcel = new Parcel(data);
		
		new ParcelProgress({
			parcel: parcel,
			oncomplete: start
		});
		
		return parcel;
	}
	
	private function start(parcel:Parcel)
	{
		EntityManager.initialize();
		
		stateMachine = new States({ name: "stateMachine" });
		playState = new PlayState({ name: GameStates.PLAY });
		
		stateMachine.add(playState);
		stateMachine.set(GameStates.PLAY);
	}
}
