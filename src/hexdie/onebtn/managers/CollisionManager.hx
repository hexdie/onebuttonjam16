package hexdie.onebtn.managers;
import hexdie.onebtn.components.PlayerControls;
import hexdie.onebtn.entity.EntityBase;
import hexdie.onebtn.managers.EntityManager;
import hexdie.onebtn.event.GameEvents;
import hexdie.onebtn.states.PlayState;
import luxe.collision.Collision;

/**
 * ...
 * @author 
 */
class CollisionManager
{
	var player:EntityBase;
	var playerControls:PlayerControls;

	public function new() 
	{
		player = EntityManager.instance.get("player");
		playerControls = player.get("controls");
	}
	
	public function update()
	{
		if (PlayState.props.playerHealth > 0)
		{
			if (!playerControls.dashing)
			{
				var initialVelocityY = player.velocity.y;
				var platforms:Array<EntityBase> = EntityManager.instance.getEntitiesWithTag("platform");
				
				for (platform in platforms)
				{
					var collision = Collision.shapeWithShape(player.hitbox, platform.hitbox);
					
					if (collision != null && collision.separation.y < 0)
					{
						player.hitbox.position.add(collision.separation);
						player.pos.add(collision.separation);
						
						if (player.velocity.y > 0)
						{
							player.velocity.y = 0;
						}
						
						if (initialVelocityY > 0 && playerControls.inAir )
						{
							Luxe.events.fire(GameEvents.PLAYER_LANDED, {platformName: platform.name});
						}
					}
				}
			}
			
			var obstacles:Array<EntityBase> = EntityManager.instance.getEntitiesWithTag("obstacle");
			
			if (!Luxe.camera.shaking)
			{
				for (obstacle in obstacles)
				{
					var collision = Collision.shapeWithShape(player.hitbox, obstacle.hitbox);
					
					if (collision != null)
					{
						Luxe.camera.shake(10);
						PlayState.props.playerHealth -= 1;
						Luxe.audio.play(Luxe.resources.audio("assets/audio/hit.mp3").source);
						
						if (PlayState.props.playerHealth <= 0)
						{
							Luxe.events.fire(GameEvents.PLAYER_DEATH);
						}
					}
				}
			}
		}
	}
	
}