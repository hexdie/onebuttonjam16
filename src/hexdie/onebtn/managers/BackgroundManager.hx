package hexdie.onebtn.managers;
import luxe.Sprite;
import luxe.Vector;
import luxe.Vector.Vec;
import phoenix.Texture.FilterType;

/**
 * ...
 * @author 
 */
class BackgroundManager
{
	var backgrounds:Array<Sprite>;

	public function new() 
	{
		backgrounds = new Array<Sprite>();
		
		var initialPos = new Vector();
		initialPos.copy_from(Luxe.camera.pos);
		
		for (i in 0...3)
		{
			backgrounds.push(createBackground(initialPos));
			initialPos.x += backgrounds[0].size.x;
		}
	}
	
	private function createBackground(pos:Vector):Sprite
	{
		var _pos = new Vector();
		_pos.copy_from(pos);
		
		var texture = Luxe.resources.texture("assets/images/background.png");
		texture.filter_mag = texture.filter_min = FilterType.nearest;
		
		var sprite = new Sprite({
			name: "background-" + backgrounds.length,
			texture: texture,
			size: new Vector(896, 504),
			pos: _pos,
			centered: false
		});
		
		return sprite;
	}
	
	public function update()
	{
		for (background in backgrounds)
		{
			if (Luxe.camera.world_point_to_screen(background.pos).x + background.size.x < 0)
			{
				var furthestBackgroundX:Float = background.pos.x;
				
				for (_b in backgrounds)
				{
					if (background != _b)
					{
						furthestBackgroundX = Math.max(furthestBackgroundX, _b.pos.x);
					}
				}
				
				background.pos.x = furthestBackgroundX + background.size.x;
			}
		}
	}
	
}