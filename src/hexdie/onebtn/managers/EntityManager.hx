package hexdie.onebtn.managers;

import haxe.PosInfos;
import hexdie.onebtn.entity.EntityBase;
import luxe.Entity;
import luxe.Scene;

/**
 * ...
 * @author 
 */
class EntityManager extends Scene
{
	public static var instance(default, null):EntityManager;
	
	private function new() 
	{
		super("main");
	}
	
	public static function initialize()
	{
		instance = new EntityManager();
	}
	
	public function getEntitiesWithTag(tag:String):Array<EntityBase>
	{
		return getEntitiesWithTags([tag]);
	}
	
	public function getEntitiesWithTags(tags:Array<String>):Array<EntityBase>
	{
		var matchingEntities = new Array<EntityBase>();
		
		for (e in entities.iterator())
		{
			for (tag in tags)
			{
				if (Reflect.getProperty(e, "tags") != null)
				{
					var entity:EntityBase = cast e;
					
					if (entity.tags.indexOf(tag) > -1)
					{
						matchingEntities.push(entity);
						break;
					}
				}
			}
		}
		
		return matchingEntities;
	}
}