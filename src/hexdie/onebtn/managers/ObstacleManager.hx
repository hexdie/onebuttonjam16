package hexdie.onebtn.managers;
import hexdie.onebtn.entity.Circler;
import hexdie.onebtn.entity.Platform;
import hexdie.onebtn.entity.Player;
import hexdie.onebtn.entity.ProjectileLauncher;
import hexdie.onebtn.event.GameEvents;
import hexdie.onebtn.event.LandingEvent;
import hexdie.onebtn.states.PlayState;
import luxe.Color;
import luxe.Vector;

/**
 * ...
 * @author 
 */

typedef ObstacleGenerationParameters = 
{
	var fn:Int->Void;
	var freq:Float;
}
 
class ObstacleManager
{
	var platformManager:PlatformManager;
	var launchers:Array<ProjectileLauncher>;
	var circlers:Array<Circler>;
	var platformsChanced:Array<Platform>;
	var player:Player;
	
	public function new(platformManager:PlatformManager) 
	{
		launchers = new Array<ProjectileLauncher>();
		circlers = new Array<Circler>();
		platformsChanced = new Array<Platform>();
		player = EntityManager.instance.get("player");
		
		this.platformManager = platformManager;
		platformsChanced.push(platformManager.platforms[0]);
		
		var currentPlatformName = platformManager.platforms[0].name;
		
		Luxe.events.listen(GameEvents.PLAYER_LANDED, function(e:LandingEvent){
			if (e.platformName != currentPlatformName)
			{
				currentPlatformName = e.platformName;
				
				PlayState.props.score += 1;
				var score = PlayState.props.score;
				
				if (score % 10 == 0)
				{
					PlayState.props.cameraSpeed *= 1.05;
					PlayState.props.projectileDistance *= 0.95;
					PlayState.props.projectileSpeed *= 1.05;
					
					if (PlayState.props.cameraSpeed > 6)
					{
						PlayState.props.cameraSpeed = 6;
					}
					
					if (PlayState.props.projectileDistance > 250)
					{
						PlayState.props.projectileDistance = 250;
					}
					
					if (PlayState.props.projectileSpeed > 8)
					{
						PlayState.props.projectileSpeed = 8;
					}
				}
			}
		});
	}
	
	public function update()
	{
		cleanupEntities();
		
		generateObstacles([
			{fn: createLauncher, freq: PlayState.props.launcherFrequency},
			{fn: createCirclers, freq: PlayState.props.circlerFrequency}
		]);
		
		#if debug
		drawEntityHitboxes();
		#end
	}
	
	private function drawEntityHitboxes()
	{
		for (e in circlers)
		{
			PlayState.drawHitbox(cast e.hitbox, new Color().rgb(0xFF4444));
		}
		
		for (launcher in launchers)
		{
			for (e in launcher.projectiles)
			{
				PlayState.drawHitbox(cast e.hitbox, new Color().rgb(0xFF4444));
			}
		}
	}
	
	private function generateObstacles(callbacks:Array<ObstacleGenerationParameters>)
	{
		for (i in 0...platformManager.platforms.length - 1)
		{
			if (platformsChanced.indexOf(platformManager.platforms[i]) < 0)
			{
				platformsChanced.push(platformManager.platforms[i]);
				
				for (callback in callbacks)
				{
					var spawnChance = Math.random();
					
					if (spawnChance <= callback.freq)
					{
						callback.fn(i);
						break;
					}
				}
			}
		}
	}
	
	private function createCirclers(platformIndex:Int)
	{
		var angle = PlayState.props.circlerAngle + (Math.random() * 0.1);
		circlers.push(new Circler(platformManager.platforms[platformIndex], angle));
	}
	
	private function createLauncher(platformIndex:Int)
	{
		var xpos = (platformManager.platforms[platformIndex].pos.x + platformManager.platforms[platformIndex+1].pos.x) / 2;
		var ypos = Luxe.camera.screen_point_to_world(new Vector(0, Luxe.camera.viewport.h)).y - Luxe.camera.pos.y;
		
		var speed = PlayState.props.projectileSpeed;
		speed = speed - (Math.random() * (speed / 2));
		
		var distance = PlayState.props.projectileDistance;
		distance = distance - (Math.random() * (distance / 2));
		
		var startDelay = Math.random();
		
		launchers.push(new ProjectileLauncher(new Vector(xpos, ypos), speed, distance, startDelay));
		
		//if (platformIndex <= platformManager.platforms.length - 2)
		//{
			//platformsChanced.push(platformManager.platforms[platformIndex + 1]);
		//}
	}
	
	private function cleanupEntities()
	{
		//clean platforms
		var temp:Array<Platform> = platformsChanced.copy();
		
		for (i in 0...platformsChanced.length)
		{
			if (platformsChanced[i] == null || platformsChanced[i].destroyed)
			{
				temp = temp.slice(0, i).concat(temp.slice(i + 1));
			}
		}
		
		platformsChanced = temp;
		
		//clean circlers
		var index = circlers.length -1;
		
		while (index >= 0)
		{
			var circler:Circler = circlers[index];
			
			if (Luxe.camera.world_point_to_screen(circler.pos).x < -50)
			{
				circlers.remove(circler);
				circler.destroy(true);
			}
			
			index -= 1;
		}
		
		//clean launchers
		index = launchers.length -1;
		
		while (index >= 0)
		{
			var launcher:ProjectileLauncher = launchers[index];
			
			if (Luxe.camera.world_point_to_screen(launcher.pos).x < -50)
			{
				launchers.remove(launcher);
				launcher.destroy(true);
			}
			
			index -= 1;
		}
	}
}