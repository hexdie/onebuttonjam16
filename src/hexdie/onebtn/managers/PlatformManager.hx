package hexdie.onebtn.managers;
import hexdie.onebtn.entity.EntityBase;
import hexdie.onebtn.entity.Platform;
import hexdie.onebtn.event.GameEvents;
import luxe.Vector;

/**
 * ...
 * @author 
 */
class PlatformManager
{
	public var platforms(default, null):Array<Platform>;
	var platformDistance:Float;
	
	public function new(startX:Float) 
	{
		platforms = new Array<Platform>();
		platformDistance = 210;
		
		for (i in 0...10)
		{
			var x = startX + (i * platformDistance);
			var y = 300 - Std.random(150);
			platforms.push(new Platform(new Vector(x, y)));
		}
		
		Luxe.events.listen(GameEvents.PLAYER_LANDED, function(_){
			generatePlatform();
			destroyPassedPlatforms();
		});
	}
	
	private function generatePlatform()
	{
		var x = platforms[platforms.length - 1].pos.x + platformDistance;
		var y = 300 - Std.random(150);
		platforms.push(new Platform(new Vector(x, y)));
	}
	
	private function destroyPassedPlatforms()
	{
		for (p in platforms)
		{
			var platform:Platform = cast p;
			var screenpos = Luxe.camera.world_point_to_screen(platform.pos);
			
			if (screenpos.x < -platformDistance)
			{
				platforms.remove(platform);
				platform.destroy(true);
			}
		}
	}
}